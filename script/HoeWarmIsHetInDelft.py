# -*- coding: utf-8 -*-
import warnings
import os

from selenium import webdriver

warnings.filterwarnings('ignore')


def get_temperature() -> str:
    """Return temperature from https://weerindelft.nl"""
    driver = webdriver.PhantomJS(executable_path=DRIVER_PATH)

    driver.get("https://weerindelft.nl")

    iframe = driver.find_element_by_id("ifrm_3")
    dashboard_link = iframe.get_attribute("src")

    # follow the dashboard link
    driver.get(dashboard_link)

    ajax_temp_element = driver.find_element_by_id("ajaxtemp")
    ajax_temp = ajax_temp_element.text

    driver.close()

    return ajax_temp


def format_temp(temperature: str) -> str:
    """Format temperature according to requirement"""

    temperature_array = temperature.split(u"\N{DEGREE SIGN}")
    temperature = round(float(temperature_array[0]))
    degrees = "degree" if temperature in [-1, 1] else "degrees"
    return f"{temperature} {degrees} Celsius"


def main():

    ajax_temp_symbol = get_temperature()
    ajax_temp = format_temp(ajax_temp_symbol)

    print(f"{ajax_temp}")


if __name__ == "__main__":

    phantomjs_path = os.getenv("PHANTOMJS_PATH")
    if phantomjs_path:
        DRIVER_PATH = phantomjs_path
    else:
        raise EnvironmentError("Please set environment variable PHANTOMJS_PATH(and point it to phantomjs binary)")

    main()
