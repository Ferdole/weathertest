## Script
- uses PhantomJS as a webdriver
- retrieves temperature from [weerindelft.nl](http://www.weerindelft.nl/) and prints xx degree/s Celsius

## Dockerfile
- used to run the script
  - built from an ubuntu image
  - PhantomJS is added
  - script gets added to the container
  - the script gets called as the only command of the container
  